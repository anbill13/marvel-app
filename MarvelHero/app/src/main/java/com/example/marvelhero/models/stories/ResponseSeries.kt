package com.example.marvelhero.models.stories

data class ResponseStories(
        val code: Int,
        val etag: String,
        val data: DataStories,
)