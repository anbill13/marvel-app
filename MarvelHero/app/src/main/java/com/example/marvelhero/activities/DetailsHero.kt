package com.example.marvelhero.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.adapters.ComicsAdapter
import com.example.marvelhero.adapters.EventsAdapter
import com.example.marvelhero.adapters.SeriesAdapter
import com.example.marvelhero.adapters.StoriesAdapter
import com.example.marvelhero.extensions.load
import com.example.marvelhero.fragments.DetailsComics
import com.example.marvelhero.fragments.DetailsEvents
import com.example.marvelhero.fragments.DetailsSeries
import com.example.marvelhero.fragments.DetailsStories
import com.example.marvelhero.interfacebuttons.ClickListener
import com.example.marvelhero.models.comics.Comics
import com.example.marvelhero.models.events.Events
import com.example.marvelhero.models.series.Series
import com.example.marvelhero.models.stories.Stories
import com.example.marvelhero.utils.Constants
import com.example.marvelhero.viewmodel.ComicsViewModel
import com.example.marvelhero.viewmodel.EventsViewModel
import com.example.marvelhero.viewmodel.SeriesViewModel
import com.example.marvelhero.viewmodel.StoriesViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel


class DetailsHero : AppCompatActivity() {
    private var mRecyclerView: RecyclerView? = null
    private lateinit var mAdapter: ComicsAdapter

    private var mRecyclerViewSeries: RecyclerView? = null
    private lateinit var mAdapterSeries: SeriesAdapter

    private var mRecyclerViewStories: RecyclerView? = null
    private lateinit var mAdapterStories: StoriesAdapter

    private var mRecyclerViewEvents: RecyclerView? = null
    private var mAdapterEvents: EventsAdapter? = null

    private var layoutComics: LinearLayout? = null
    private var layoutSeries: LinearLayout? = null
    private var layoutStories: LinearLayout? = null
    private var layoutEvent: LinearLayout? = null

    private var statesHideComics = false
    private var statesHideSeries = false
    private var statesHideStories = false
    private var statesHideEvents = false

    private val viewModel: ComicsViewModel by lazy {
        ViewModelProviders.of(this).get(ComicsViewModel::class.java)
    }
    private val viewModelEvents: EventsViewModel by lazy {
        ViewModelProviders.of(this).get(EventsViewModel::class.java)
    }
    private val viewModelSeries: SeriesViewModel by lazy {
        ViewModelProviders.of(this).get(SeriesViewModel::class.java)
    }
    private val viewModelStories: StoriesViewModel by lazy {
        ViewModelProviders.of(this).get(StoriesViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_hero)

        shapeButton()
        setUpRecyclerViewComics()
        setUpRecyclerViewSeries()
        setUpRecyclerViewStories()
        setUpRecyclerViewEvents()

    }

    private fun setUpRecyclerViewComics() {
        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mAdapter = ComicsAdapter(object : ClickListener<Comics> {
            override fun onClick(view: View, item: Comics, position: Int) {

                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

            }
        })
        mRecyclerView!!.adapter = mAdapter
    }

    private fun setUpRecyclerViewSeries() {
        mRecyclerViewSeries = findViewById(R.id.seriesRecyclerview)
        mRecyclerViewSeries!!.setHasFixedSize(true)
        mRecyclerViewSeries!!.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mAdapterSeries = SeriesAdapter(object : ClickListener<Series> {
            override fun onClick(view: View, item: Series, position: Int) {

                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

            }
        })
        mRecyclerViewSeries!!.adapter = mAdapterSeries
    }

    private fun setUpRecyclerViewStories() {
        mRecyclerViewStories = findViewById(R.id.storiesRecyclerview)
        mRecyclerViewStories!!.setHasFixedSize(true)
        mRecyclerViewStories!!.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mAdapterStories = StoriesAdapter(object : ClickListener<Stories> {
            override fun onClick(view: View, item: Stories, position: Int) {

                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

            }
        })
        mRecyclerViewStories!!.adapter = mAdapterStories
    }

    private fun setUpRecyclerViewEvents() {
        mRecyclerViewEvents = findViewById(R.id.eventRecyclerview)
        mRecyclerViewEvents!!.setHasFixedSize(true)
        mRecyclerViewEvents!!.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mAdapterEvents = EventsAdapter(object : ClickListener<Events> {
            override fun onClick(view: View, item: Events, position: Int) {

                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

            }
        })
        mRecyclerViewEvents!!.adapter = mAdapterEvents
    }

    private fun shapeButton() {


        val radius = resources.getDimension(R.dimen.mtrl_calendar_selection_text_baseline_to_bottom)
        val linearLayout: FrameLayout = findViewById(R.id.layout)

        layoutComics = findViewById(R.id.layoutComics)
        layoutSeries = findViewById(R.id.layoutSerie)
        layoutStories = findViewById(R.id.layoutStories)
        layoutEvent = findViewById(R.id.layoutEvent)

        val textButton: TextView = findViewById(R.id.textButton)
        val nameHero: TextView = findViewById(R.id.nameHeroDetail)
        val descriptionHero: TextView = findViewById(R.id.descriptionHero)
        val back: ImageView = findViewById(R.id.back)
        val imageHero: ImageView = findViewById(R.id.imageHeroDetail)


        val viewMoreComics: TextView = findViewById(R.id.viewMoreComics)
        val viewMoreSeries: TextView = findViewById(R.id.viewMoreSeries)
        val viewMoreStories: TextView = findViewById(R.id.viewMoreStories)
        val viewMoreEvents: TextView = findViewById(R.id.viewMoreEvents)

        val hideComics: ImageView = findViewById(R.id.hideComics)
        val hideSeries: ImageView = findViewById(R.id.hideSeries)
        val hideStories: ImageView = findViewById(R.id.hideStories)
        val hideEvent: ImageView = findViewById(R.id.hideEvents)


        val name = intent.extras!!.getString("NAME_HERO")
        val description = intent.extras!!.getString("DESCRIPTION_HERO")
        val image = intent.extras!!.getString("IMAGE_HERO")
        val heroId = intent.extras!!.getString("IDENTIFICATOR")
        var url = intent.extras!!.getString("URLS")

        if (heroId != null) {
            initObserver(heroId.toInt())
        }

        if (description.isNullOrEmpty()) {
            descriptionHero.text = "The description of this hero wasn't found"
        } else {
            descriptionHero.text = description
        }

        imageHero.load(image!!)
        nameHero.text = name
        textButton.text = "READ PROFILE"
        linearLayout.isClickable = true

        back.setOnClickListener {
            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            onBackPressed()
        }


        hideComics.setOnClickListener {

            statesHideComics =
                hideCategories(hideComics, statesHideComics, mRecyclerView, it)

        }
        viewMoreComics.setOnClickListener {

            val nameHero = intent.extras!!.getString("NAME_HERO")
            val idHero = intent.extras!!.getString("IDENTIFICATOR")

            val bundle = Bundle()
            bundle.putString("NAME_HERO", nameHero)
            bundle.putString("IDENTIFICATOR", idHero)

            val mFragment = DetailsComics()
            mFragment.arguments = bundle

            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragmentHolder, mFragment, "TAG")
            transaction.addToBackStack(null)
            transaction.commit()

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        }

        hideSeries.setOnClickListener {
            statesHideSeries =
                hideCategories(hideSeries, statesHideSeries, mRecyclerViewSeries, it)


        }
        viewMoreSeries.setOnClickListener {
            val nameHero = intent.extras!!.getString("NAME_HERO")
            val idHero = intent.extras!!.getString("IDENTIFICATOR")

            val bundle = Bundle()
            bundle.putString("NAME_HERO", nameHero)
            bundle.putString("IDENTIFICATOR", idHero)

            val mFragment = DetailsSeries()
            mFragment.arguments = bundle

            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragmentHolder, mFragment, "TAG")
            transaction.addToBackStack(null)
            transaction.commit()

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        }

        hideStories.setOnClickListener {
            statesHideStories =
                hideCategories(hideStories, statesHideStories, mRecyclerViewStories, it)

        }
        viewMoreStories.setOnClickListener {

            val nameHero = intent.extras!!.getString("NAME_HERO")
            val idHero = intent.extras!!.getString("IDENTIFICATOR")

            val bundle = Bundle()
            bundle.putString("NAME_HERO", nameHero)
            bundle.putString("IDENTIFICATOR", idHero)
            val mFragment = DetailsStories()
            mFragment.arguments = bundle

            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragmentHolder, mFragment, "TAG")
            transaction.addToBackStack(null)
            transaction.commit()

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        }

        hideEvent.setOnClickListener {
            statesHideEvents =
                hideCategories(hideEvent, statesHideEvents, mRecyclerViewEvents, it)


        }
        viewMoreEvents.setOnClickListener {


            val idHero = intent.extras!!.getString("IDENTIFICATOR")

            val bundle = Bundle()
            bundle.putString("NAME_HERO", name)
            bundle.putString("IDENTIFICATOR", idHero)
            bundle.putString("DESCRIPTION_HERO", description)

            val mFragment = DetailsEvents()
            mFragment.arguments = bundle

            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragmentHolder, mFragment, "TAG")
            transaction.addToBackStack(null)
            transaction.commit()

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        }

        val shapeAppearanceModel = ShapeAppearanceModel()
            .toBuilder()
            .setTopLeftCorner(CornerFamily.CUT, radius)
            .setBottomRightCorner(CornerFamily.CUT, radius)
            .build()
        val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

        ViewCompat.setBackground(linearLayout, shapeDrawable)

        linearLayout.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            if (!url.isNullOrEmpty()) intent.data = Uri.parse("$url")
            else url = Constants.EMPTY_URL
            startActivity(intent)
            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
        }
    }

    private fun initObserver(characterId: Int) {
        viewModel.getComics(characterId).observe(this, {
            val shimmerFrameLayoutComics: ShimmerFrameLayout =
                findViewById(R.id.shimmerFrameLayoutComics)
            shimmerFrameLayoutComics.isShimmerStarted
            shimmerFrameLayoutComics.stopShimmer()
            shimmerFrameLayoutComics.visibility = View.GONE

            mAdapter.updateComicsList(it)
            if (it.isEmpty()) {
                mRecyclerView?.visibility = View.GONE
                layoutComics?.visibility = View.GONE

            }
        })

        viewModelEvents.getEvents(characterId).observe(this, {
            val shimmerFrameLayoutEvents: ShimmerFrameLayout =
                findViewById(R.id.shimmerFrameLayoutEvents)
            shimmerFrameLayoutEvents.isShimmerStarted
            shimmerFrameLayoutEvents.stopShimmer()
            shimmerFrameLayoutEvents.visibility = View.GONE

            mAdapterEvents!!.updateEventsList(it)
            if (it.isEmpty()) {
                mRecyclerViewEvents?.visibility = View.GONE
                layoutEvent?.visibility = View.GONE
            }

        })

        viewModelSeries.getSeries(characterId).observe(this, {
            val shimmerFrameLayoutSeries: ShimmerFrameLayout =
                findViewById(R.id.shimmerFrameLayoutSeries)
            shimmerFrameLayoutSeries.isShimmerStarted
            shimmerFrameLayoutSeries.stopShimmer()
            shimmerFrameLayoutSeries.visibility = View.GONE

            mAdapterSeries.updateSeriesList(it)
            if (it.isEmpty()) {
                mRecyclerViewSeries?.visibility = View.GONE
                layoutSeries?.visibility = View.GONE

            }
        })

        viewModelStories.getStories(characterId).observe(this, {
            val shimmerFrameLayoutStories: ShimmerFrameLayout =
                findViewById(R.id.shimmerFrameLayoutStories)
            shimmerFrameLayoutStories.isShimmerStarted
            shimmerFrameLayoutStories.stopShimmer()
            shimmerFrameLayoutStories.visibility = View.GONE

            mAdapterStories.updateStoriesList(it)
            if (it.isEmpty()) {
                mRecyclerViewStories?.visibility = View.GONE
                layoutSeries?.visibility = View.GONE

            }
        })
    }


    private fun hideCategories(
        imageView: ImageView,
        state: Boolean,
        recyclerView: RecyclerView?,
        it: View,
    ): Boolean {

        if (!state) {
            imageView.setImageResource(R.drawable.ic_arrow_down)
            recyclerView?.visibility = View.GONE
            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        } else {

            imageView.setImageResource(R.drawable.ic_arrow_up)
            recyclerView?.visibility = View.VISIBLE
            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

        }
        return !state
    }
}