package com.example.marvelhero.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marvelhero.api.AppModule
import com.example.marvelhero.models.events.Events
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class EventsViewModel : ViewModel() {


    var isLoading: Boolean = false
        private set

    fun getEvents(characterId: Int): LiveData<List<Events>> {
        val comicList = MutableLiveData<List<Events>>()

        GlobalScope.launch {
            isLoading = true

            val result = AppModule.getEvents(characterId).await()

            comicList.postValue(result.data.results)
        }
        isLoading = false
        return comicList
    }


}