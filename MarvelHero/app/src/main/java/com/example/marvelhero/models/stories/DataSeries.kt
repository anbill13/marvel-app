package com.example.marvelhero.models.stories


data class DataStories(
        val offset: Int,
        val limit: Int,
        val total: Int,
        val count: Int,
        val results: List<Stories>
)