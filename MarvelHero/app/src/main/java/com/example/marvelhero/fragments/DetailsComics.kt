package com.example.marvelhero.fragments

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.adapters.ComicsAdapter
import com.example.marvelhero.extensions.load
import com.example.marvelhero.interfacebuttons.ClickListener
import com.example.marvelhero.models.comics.Comics
import com.example.marvelhero.utils.Constants
import com.example.marvelhero.viewmodel.ComicsViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import java.text.SimpleDateFormat
import java.util.*


class DetailsComics : Fragment() {

    private lateinit var urls: String
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: ComicsAdapter
    private val viewModel: ComicsViewModel by lazy {
        ViewModelProviders.of(this).get(ComicsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_others_details_hero, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerViewComics(view)
        shapeButton(view)


    }

    private fun setUpRecyclerViewComics(view: View) {

        mRecyclerView = view.findViewById(R.id.recyclerView)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = GridLayoutManager(view.context, 2)
        mAdapter = ComicsAdapter( object :ClickListener<Comics>{
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onClick(view: View, item: Comics, position: Int) {

                val dialog = Dialog(this@DetailsComics.requireContext())
                dialog.setContentView(R.layout.fragment_preview_details_categories)

                val titlePreviewComics: TextView = dialog.findViewById(R.id.toolbarCategories)
                val format: TextView = dialog.findViewById(R.id.format)
                val datePublished: TextView = dialog.findViewById(R.id.datePublished)
                val pages: TextView = dialog.findViewById(R.id.peges)
                val language: TextView = dialog.findViewById(R.id.language)
                val editor: TextView = dialog.findViewById(R.id.editor)
                val inker: TextView = dialog.findViewById(R.id.inker)
                val writer: TextView = dialog.findViewById(R.id.writer)

                val descriptionPreviewComics: TextView =
                    dialog.findViewById(R.id.descriptionPreviewComics)
                val imagePreviewComics: ImageView = dialog.findViewById(R.id.imagePreviewComics)
                val textButton: TextView = dialog.findViewById(R.id.textButton)
                val back: ImageView = dialog.findViewById(R.id.back)
                textButton.text = "Know More"

                back.visibility = View.GONE
                titlePreviewComics.text = item.title
                titlePreviewComics.gravity = Gravity.CENTER
                titlePreviewComics.textSize = 18f
                descriptionPreviewComics.text = item.description
//                    .replace("<br>"," ")
                format.text = item.format
                pages.text = item.pageCount.toString()
                val getData = item.dates[0].date

                val parser = SimpleDateFormat("yyyy-MM-dd")
                val formatter = SimpleDateFormat("dd.MM.yyyy")
                parser.timeZone = TimeZone.getTimeZone("UTC")
                val output = formatter.format(parser.parse(getData))

                datePublished.text = output

                if (item.textObjects.isNotEmpty()) language.text = item.textObjects[0].language

                val editors = item.creators.items.filter { it.role == "editor" }
                val inkers = item.creators.items.filter { it.role == "inker" }
                val writers = item.creators.items.filter { it.role == "writer" }

                if (editors.isNotEmpty()) {
                    if (editors[0].role == "editor") editor.text = editors[0].name

                } else editor.text = "Editor no found"

                if (inkers.isNotEmpty()) {

                    if (inkers[0].role == "inker") inker.text = inkers[0].name

                } else inker.text = "Inker no found"

                if (writers.isNotEmpty()) {

                    if (writers[0].role == "writer") writer.text = writers[0].name

                } else writer.text = "writer no found"

                val thumbnailUri = item.thumbnail.path + "." + item.thumbnail.extension
                val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)
                imagePreviewComics.load(url)

                titlePreviewComics.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28F)
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

                val radius =
                    resources.getDimension(R.dimen.mtrl_calendar_selection_text_baseline_to_bottom)

                val linearLayout = dialog.findViewById<FrameLayout>(R.id.layout)
                linearLayout.isClickable = true

                val shapeAppearanceModel = ShapeAppearanceModel()
                    .toBuilder()
                    .setTopLeftCorner(CornerFamily.CUT, radius)
                    .setBottomRightCorner(CornerFamily.CUT, radius)
                    .build()
                val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

                ViewCompat.setBackground(linearLayout, shapeDrawable)

                linearLayout.setOnClickListener { view ->

                    val urlComics = item.urls.filter { it.type == "detail" }
                    val urlEmptyComics = Constants.EMPTY_URL

                    urls = if (urlComics.isEmpty() || urlComics[0].type != "detail") urlEmptyComics
                    else urlComics[0].url

                    val intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.addCategory(Intent.CATEGORY_BROWSABLE)
                    intent.data = Uri.parse(urls)
                    startActivity(intent)
                    view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                }

                dialog.show()
            }
        })
        mRecyclerView.adapter = mAdapter
    }

    private fun shapeButton(view: View) {

        val textButton = view.findViewById<TextView>(R.id.toolbarCategories)
        val nameHero = requireArguments().getString("NAME_HERO")
        val idHero = requireArguments().getString("IDENTIFICATOR")
        textButton.text = "${nameHero}'S COMICS"
        initObserver(idHero!!.toInt(), view)
        textButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28F)
        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

    }

    private fun initObserver(characterId: Int, view: View) {
        viewModel.getComics(characterId).observe(viewLifecycleOwner, {
            val shimmerFrameLayout: ShimmerFrameLayout = view.findViewById(R.id.shimmerFrameLayout)
            shimmerFrameLayout.isShimmerStarted
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
            mAdapter.updateComicsList(it)
        })
    }

}