package com.example.marvelhero.models.series

import com.example.marvelhero.models.Creators
import com.example.marvelhero.models.Thumbnail
import com.example.marvelhero.models.Url


data class Series(
        val id: Int,
        val title: String,
        val startYear: String,
        val endYear: String,
        val rating: String,
        val description: String,
        val thumbnail: Thumbnail,
        val creators: Creators,
        val urls: Array<Url>,
        ) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Series

                if (id != other.id) return false
                if (title != other.title) return false
                if (startYear != other.startYear) return false
                if (endYear != other.endYear) return false
                if (rating != other.rating) return false
                if (description != other.description) return false
                if (thumbnail != other.thumbnail) return false
                if (creators != other.creators) return false
                if (!urls.contentEquals(other.urls)) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id
                result = 31 * result + title.hashCode()
                result = 31 * result + startYear.hashCode()
                result = 31 * result + endYear.hashCode()
                result = 31 * result + rating.hashCode()
                result = 31 * result + description.hashCode()
                result = 31 * result + thumbnail.hashCode()
                result = 31 * result + creators.hashCode()
                result = 31 * result + urls.contentHashCode()
                return result
        }
}