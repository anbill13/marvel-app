package com.example.marvelhero.models.stories

data class Stories(
    val id: Int,
    val title: String,
    val description: String,
    val pageCount: Int,
    val resourceURI: String,
)