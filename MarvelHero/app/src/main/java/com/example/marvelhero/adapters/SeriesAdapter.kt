package com.example.marvelhero.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.series.Series
import com.example.marvelhero.extensions.load
import com.example.marvelhero.holders.SeriesViewHolder
import com.example.marvelhero.interfacebuttons.ClickListener

class SeriesAdapter(
    private val clickListener: ClickListener<Series>,
) : RecyclerView.Adapter<SeriesViewHolder>() {

    private val series = mutableListOf<Series>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_row_comics,
            parent,
            false
        )
        return SeriesViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        val serie = series[position]

        val thumbnailUri = serie.thumbnail.path + "." + serie.thumbnail.extension
        val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)

        holder.item = serie
        holder.name.text = serie.title
        holder.resourceURI.load(url)
    }


    override fun getItemCount(): Int = series.size

    fun updateSeriesList(series: List<Series>) {
        this.series.clear()
        this.series.addAll(series)
        notifyDataSetChanged()
    }
}
