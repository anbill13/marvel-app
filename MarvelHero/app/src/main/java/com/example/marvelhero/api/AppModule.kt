package com.example.marvelhero.api

import com.example.marvelhero.extensions.md5
import com.example.marvelhero.utils.Constants
import com.example.marvelhero.utils.Constants.API_KEY
import com.example.marvelhero.utils.Constants.PRIVATE_KEY
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

object AppModule {
    private val API: ApiServise by lazy {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val ts =
                (Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis / 1000L).toString()
            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("apikey", API_KEY)
                .addQueryParameter("ts", ts)
                .addQueryParameter("hash","$ts$PRIVATE_KEY$API_KEY".md5())
                .build()
            val requestBuilder = original.newBuilder().url(url)
            val request = requestBuilder.build()
            chain.proceed(request)

        }
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()

        retrofit.create(ApiServise::class.java)

    }

    fun getCharacters() = API.getCharacter(Constants.LIMIT)
    fun getComics(characterId: Int) = API.getComics(characterId, Constants.LIMIT)
    fun getEvents(characterId: Int) = API.getEvents(characterId, Constants.LIMIT)
    fun getSeries(characterId: Int) = API.getSeries(characterId, Constants.LIMIT)
    fun getStories(characterId: Int) = API.getStories(characterId, Constants.LIMIT)

}




