package com.example.marvelhero.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.series.Series
import com.example.marvelhero.interfacebuttons.ClickListener

class SeriesViewHolder(itemView: View, listener: ClickListener<Series>) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

    lateinit var item: Series
    val resourceURI = itemView.findViewById(R.id.imageComics) as ImageView
    val name = itemView.findViewById(R.id.titleComics) as TextView

        private val clickListener = listener

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {

            clickListener.onClick(v!!, item,adapterPosition)
        }

}