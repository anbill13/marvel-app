package com.example.marvelhero.models.series

data class ResponseSeries(
        val code: Int,
        val etag: String,
        val data: DataSeries
)