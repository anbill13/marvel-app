package com.example.marvelhero.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.events.Events
import com.example.marvelhero.extensions.load
import com.example.marvelhero.holders.EventsViewHolder
import com.example.marvelhero.interfacebuttons.ClickListener

class EventsAdapter(
    private val clickListener: ClickListener<Events>,
) : RecyclerView.Adapter<EventsViewHolder>() {

    private val events = mutableListOf<Events>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_row_comics,
            parent,
            false
        )
        return EventsViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        val event = events[position]
        val thumbnailUri = event.thumbnail.path + "." + event.thumbnail.extension
        val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)

        holder.item = event
        holder.name.text = event.title
        holder.resourceURI.load(url)
    }


    override fun getItemCount(): Int = events.size

    fun updateEventsList(events: List<Events>) {
        this.events.clear()
        this.events.addAll(events)
        notifyDataSetChanged()
    }
}
