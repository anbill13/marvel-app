package com.example.marvelhero.models.events

data class ResponseEvents(
        val code: Int,
        val etag: String,
        val data: DataEvents
)