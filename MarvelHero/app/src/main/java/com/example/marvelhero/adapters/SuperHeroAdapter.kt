package com.example.marvelhero.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.characters.Character
import com.example.marvelhero.extensions.load
import com.example.marvelhero.holders.SuperHeroViewHolder
import com.example.marvelhero.interfacebuttons.ClickListener

class SuperHeroAdapter(
    private val clickListener: ClickListener<Character>,
) : RecyclerView.Adapter<SuperHeroViewHolder>() {

    private val heroes = mutableListOf<Character>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuperHeroViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_row_all_hero,
            parent,
            false
        )
        return SuperHeroViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: SuperHeroViewHolder, position: Int) {
        val hero = heroes[position]
        val thumbnailUri = hero.thumbnail.path + "." + hero.thumbnail.extension
        val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)

        holder.item = hero;
        holder.nameHero.text = hero.name
        holder.imageHero.load(url)

    }

    override fun getItemCount(): Int = heroes.size

    fun updateHeroList(heroes: List<Character>) {
        this.heroes.clear()
        this.heroes.addAll(heroes)
        notifyDataSetChanged()
    }
}
