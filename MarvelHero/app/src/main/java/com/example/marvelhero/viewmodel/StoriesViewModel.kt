package com.example.marvelhero.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marvelhero.api.AppModule
import com.example.marvelhero.models.stories.Stories
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class StoriesViewModel : ViewModel() {


    var isLoading: Boolean = false
        private set

    fun getStories(characterId: Int): LiveData<List<Stories>> {
        val comicList = MutableLiveData<List<Stories>>()

        GlobalScope.launch {
            isLoading = true

            val result = AppModule.getStories(characterId).await()

            comicList.postValue(result.data.results)
        }
        isLoading = false
        return comicList
    }


}