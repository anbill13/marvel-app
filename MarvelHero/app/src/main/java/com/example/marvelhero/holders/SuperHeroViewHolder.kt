package com.example.marvelhero.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.characters.Character
import com.example.marvelhero.interfacebuttons.ClickListener

class SuperHeroViewHolder(itemView: View, listener: ClickListener<Character>) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
    lateinit var item: Character;
    val imageHero = itemView.findViewById(R.id.imageHero) as ImageView
    val nameHero = itemView.findViewById(R.id.nameHero) as TextView
        private val clickListener = listener

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clickListener.onClick(v!!, item, adapterPosition)
        }

}