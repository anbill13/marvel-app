package com.example.marvelhero.fragments

import android.os.Bundle
import android.view.HapticFeedbackConstants
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.DialogFragment
import com.example.marvelhero.R
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel


class PreviewDetailsCategoriesFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_preview_details_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shapeButton(view)
    }
    private fun shapeButton(view: View) {

        val radius = resources.getDimension(R.dimen.mtrl_calendar_selection_text_baseline_to_bottom)
        val linearLayout = view.findViewById<FrameLayout>(R.id.layout)
        val textButton = view.findViewById<TextView>(R.id.textButton)
        textButton.text = "Know More"
        linearLayout.isClickable = true

        val shapeAppearanceModel = ShapeAppearanceModel()
            .toBuilder()
            .setTopLeftCorner(CornerFamily.CUT, radius)
            .setBottomRightCorner(CornerFamily.CUT, radius)
            .build()
        val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

        ViewCompat.setBackground(linearLayout, shapeDrawable)

        linearLayout.setOnClickListener {

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
        }
    }


}