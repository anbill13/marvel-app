package com.example.marvelhero.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.marvelhero.R

fun ImageView.load(url: String) {
    Glide.with(context)
        .load(url)
        .error(R.drawable.clean)
        .into(this)

}