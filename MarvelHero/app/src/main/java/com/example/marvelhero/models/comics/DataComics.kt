package com.example.marvelhero.models.comics

data class DataComics(
        val offset: Int,
        val limit: Int,
        val total: Int,
        val count: Int,
        val results: List<Comics>
)