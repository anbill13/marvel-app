package com.example.marvelhero.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marvelhero.api.AppModule
import com.example.marvelhero.models.comics.Comics
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class ComicsViewModel : ViewModel() {


    var isLoading: Boolean = false
        private set

    fun getComics(characterId: Int): LiveData<List<Comics>> {
        val comicList = MutableLiveData<List<Comics>>()

        GlobalScope.launch {
            isLoading = true

            val result = AppModule.getComics(characterId).await()

            comicList.postValue(result.data.results)
        }
        isLoading = false
        return comicList
    }


}