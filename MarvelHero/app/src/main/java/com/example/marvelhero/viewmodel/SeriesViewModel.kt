package com.example.marvelhero.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marvelhero.api.AppModule
import com.example.marvelhero.models.series.Series
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class SeriesViewModel : ViewModel() {


    var isLoading: Boolean = false
        private set

    fun getSeries(characterId: Int): LiveData<List<Series>> {
        val comicList = MutableLiveData<List<Series>>()

        GlobalScope.launch {
            isLoading = true

            val result = AppModule.getSeries(characterId).await()

            comicList.postValue(result.data.results)
        }
        isLoading = false
        return comicList
    }


}