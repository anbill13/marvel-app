package com.example.marvelhero.models.series

data class DataSeries(
        val offset: Int,
        val limit: Int,
        val total: Int,
        val count: Int,
        val results: List<Series>,
)