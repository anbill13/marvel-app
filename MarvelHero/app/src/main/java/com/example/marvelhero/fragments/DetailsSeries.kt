package com.example.marvelhero.fragments

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.TypedValue
import android.view.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.adapters.SeriesAdapter
import com.example.marvelhero.extensions.load
import com.example.marvelhero.interfacebuttons.ClickListener
import com.example.marvelhero.models.series.Series
import com.example.marvelhero.utils.Constants
import com.example.marvelhero.viewmodel.SeriesViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import java.util.*

class DetailsSeries : Fragment() {

    private lateinit var urls: String
    private lateinit var mRecyclerViewSeries: RecyclerView
    private lateinit var mAdapterSeries: SeriesAdapter
    private val viewModelSeries: SeriesViewModel by lazy {
        ViewModelProviders.of(this).get(SeriesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_others_details_hero, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerViewSeries(view)
        shapeButton(view)

    }

    private fun setUpRecyclerViewSeries(view: View) {

        mRecyclerViewSeries = view.findViewById(R.id.recyclerView)
        mRecyclerViewSeries.setHasFixedSize(true)
        mRecyclerViewSeries.layoutManager = GridLayoutManager(view.context, 2)
        mAdapterSeries = SeriesAdapter( object : ClickListener<Series> {
            override fun onClick(view: View, item: Series, position: Int) {

                val dialog = Dialog(this@DetailsSeries.requireContext())
                dialog.setContentView(R.layout.fragment_preview_details_series)


                val titlePreviewComics: TextView = dialog.findViewById(R.id.toolbarCategories)

                val startYear: TextView = dialog.findViewById(R.id.startYear)
                val endYear: TextView = dialog.findViewById(R.id.endYear)
                val rating: TextView = dialog.findViewById(R.id.rating)
                val penciller: TextView = dialog.findViewById(R.id.penciller)
                val letter: TextView = dialog.findViewById(R.id.letter)
                val inker: TextView = dialog.findViewById(R.id.inker)
                val writer: TextView = dialog.findViewById(R.id.writer)

                val descriptionPreviewSeries: TextView =
                    dialog.findViewById(R.id.descriptionPreviewSeries)
                val imagePreviewSeries: ImageView = dialog.findViewById(R.id.imagePreviewSeries)

                val textButton: TextView = dialog.findViewById(R.id.textButton)
                val back: ImageView = dialog.findViewById(R.id.back)
                startYear.text = item.startYear
                endYear.text = item.endYear

                if (item.rating.isEmpty()) rating.text = "rating no found"
                else rating.text = item.rating

                val pencils = item.creators.items.filter { it.role == "penciller" }
                val letters = item.creators.items.filter { it.role == "letter" }
                val inkers = item.creators.items.filter { it.role == "inker" }
                val writers = item.creators.items.filter { it.role == "writer" }

                if (pencils.isNotEmpty()) {
                    if (pencils[0].role == "penciller") penciller.text = pencils[0].name

                } else letter.text = "letter no found"
                if (letters.isNotEmpty()) {
                    if (letters[0].role == "letter") letter.text = letters[0].name

                } else letter.text = "letter no found"
                if (inkers.isNotEmpty()) {

                    if (inkers[0].role == "inker") inker.text = inkers[0].name

                } else inker.text = "Inker no found"
                if (writers.isNotEmpty()) {

                    if (writers[0].role == "writer") writer.text = writers[0].name

                } else writer.text = "writer no found"


                textButton.text = "Know More"

                back.visibility = View.GONE
                titlePreviewComics.textSize = 18f
                titlePreviewComics.text = item.title
                titlePreviewComics.gravity = Gravity.CENTER
                descriptionPreviewSeries.text = item.description

                val thumbnailUri = item.thumbnail.path + "." + item.thumbnail.extension
                val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)
                imagePreviewSeries.load(url)

                titlePreviewComics.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28F)
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

                val radius =
                    resources.getDimension(R.dimen.mtrl_calendar_selection_text_baseline_to_bottom)

                val linearLayout = dialog.findViewById<FrameLayout>(R.id.layout)
                linearLayout.isClickable = true

                val shapeAppearanceModel = ShapeAppearanceModel()
                    .toBuilder()
                    .setTopLeftCorner(CornerFamily.CUT, radius)
                    .setBottomRightCorner(CornerFamily.CUT, radius)
                    .build()
                val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

                ViewCompat.setBackground(linearLayout, shapeDrawable)

                linearLayout.setOnClickListener { view ->

                    val urlComics = item.urls.filter { it.type == "detail" }
                    val urlEmptyComics = Constants.EMPTY_URL

                    urls = if (urlComics.isEmpty() || urlComics[0].type != "detail") urlEmptyComics
                    else urlComics[0].url

                    val intent = Intent()
                    intent.action = Intent.ACTION_VIEW
                    intent.addCategory(Intent.CATEGORY_BROWSABLE)
                    intent.data = Uri.parse(urls)
                    startActivity(intent)
                    view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                }

                dialog.show()
            }

        })
        mRecyclerViewSeries.adapter = mAdapterSeries
    }

    private fun shapeButton(view: View) {
        val textButton = view.findViewById<TextView>(R.id.toolbarCategories)

        val nameHero = requireArguments().getString("NAME_HERO")
        val idHero = requireArguments().getString("IDENTIFICATOR")
        initObserver(idHero!!.toInt(), view)

        textButton.text = "${nameHero}'S SERIES"
        textButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28F)
        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

    }

    private fun initObserver(characterId: Int, view: View) {

        viewModelSeries.getSeries(characterId).observe(viewLifecycleOwner, {
            val shimmerFrameLayout: ShimmerFrameLayout = view.findViewById(R.id.shimmerFrameLayout)
            shimmerFrameLayout.isShimmerStarted
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
            mAdapterSeries.updateSeriesList(it)
        })
    }
}