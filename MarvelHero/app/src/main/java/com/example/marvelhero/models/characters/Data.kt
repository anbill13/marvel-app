package com.example.marvelhero.models.characters

data class Data(
        val offset: Int,
        val limit: Int ,
        val total: Int,
        val count: Int,
        val results: List<Character>

)