package com.example.marvelhero.api

import com.example.marvelhero.models.characters.Response
import com.example.marvelhero.models.comics.ResponseComics
import com.example.marvelhero.models.events.ResponseEvents
import com.example.marvelhero.models.series.ResponseSeries
import com.example.marvelhero.models.stories.ResponseStories
import com.example.marvelhero.utils.Constants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServise {

    @GET(Constants.END_POINT)
    fun getCharacter(
        @Query("limit") limit: Int?

    ): Call<Response>

    @GET("${Constants.END_POINT_BY_ID}/comics")
    fun getComics(
        @Query(Constants.END_POINT) characterId: Int,
        @Query("limit") limit: Int?

    ): Call<ResponseComics>

    @GET("${Constants.END_POINT_BY_ID}/events")
    fun getEvents(
        @Query(Constants.END_POINT) characterId: Int,
        @Query("limit") limit: Int?

    ): Call<ResponseEvents>

    @GET("${Constants.END_POINT_BY_ID}/series")
    fun getSeries(
        @Query(Constants.END_POINT) characterId: Int,
        @Query("limit") limit: Int?

    ): Call<ResponseSeries>

    @GET("${Constants.END_POINT_BY_ID}/stories")
    fun getStories(
        @Query(Constants.END_POINT) characterId: Int,
        @Query("limit") limit: Int?

    ): Call<ResponseStories>
}