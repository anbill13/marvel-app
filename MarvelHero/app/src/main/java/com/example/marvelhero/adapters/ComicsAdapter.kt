package com.example.marvelhero.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.comics.Comics
import com.example.marvelhero.extensions.load
import com.example.marvelhero.holders.ComicsViewHolder
import com.example.marvelhero.interfacebuttons.ClickListener

class ComicsAdapter( private val clickListener: ClickListener<Comics> ) : RecyclerView.Adapter<ComicsViewHolder>() {

    private val comics = mutableListOf<Comics>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicsViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_row_comics,
            parent,
            false
        )
        return ComicsViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: ComicsViewHolder, position: Int) {
        val comic = comics[position]
        val thumbnailUri = comic.thumbnail.path + "." + comic.thumbnail.extension
        val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)

        holder.item = comic
        holder.name.text = comic.title
        holder.resourceURI.load(url)
    }


    override fun getItemCount(): Int = comics.size

    fun updateComicsList(comics: List<Comics>) {
        this.comics.clear()
        this.comics.addAll(comics)
        notifyDataSetChanged()
    }
}
