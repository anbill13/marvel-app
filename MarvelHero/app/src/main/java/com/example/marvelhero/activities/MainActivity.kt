package com.example.marvelhero.activities

import android.content.Intent
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.adapters.SuperHeroAdapter
import com.example.marvelhero.interfacebuttons.ClickListener
import com.example.marvelhero.models.characters.Character
import com.example.marvelhero.viewmodel.CharactersViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: SuperHeroAdapter

    private var character = arrayListOf<Character>()
    private var matchedCharacter: ArrayList<Character> = arrayListOf()


    private val viewModel: CharactersViewModel by lazy {
        ViewModelProviders.of(this).get(CharactersViewModel::class.java)
    }

    private var showSearch = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpRecyclerView()
        setupSearchView()
        shapeButton()
        initObservables()


        val search: ImageView = findViewById(R.id.searchClick)
        val hideSearch: LinearLayout = findViewById(R.id.hideSearch)

        search.setOnClickListener {
            showSearch = searchStatus(search, hideSearch, showSearch)
        }

    }

    private fun setupSearchView() {
        val searchView: SearchView = findViewById(R.id.searchHeroView)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                search(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return false
            }

        })

    }

    private fun search(text: String?) {
        matchedCharacter = arrayListOf()

        text?.let {
            character.forEach { character ->
                if (character.name.contains(text, true) ||
                    character.name.contains(text, true)
                ) {
                    matchedCharacter.add(character)
                }
            }
            updateRecyclerView()
            if (matchedCharacter.isEmpty()) {
                Toast.makeText(this, "No match found!", Toast.LENGTH_SHORT).show()
            }
            updateRecyclerView()
        }
    }

    private fun searchStatus(
        imageView: ImageView,
        linearLayout: LinearLayout,
        state: Boolean,
    ): Boolean {
        if (!state) {


            imageView.setImageResource(R.drawable.ic_round_close)
            linearLayout.visibility = View.VISIBLE

        } else {

            imageView.setImageResource(R.drawable.shr_search)
            linearLayout.visibility = View.GONE

        }
        return !state
    }

    private fun setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = GridLayoutManager(this, 2)
        mAdapter = SuperHeroAdapter(object : ClickListener<Character> {
            override fun onClick(view: View, hero: Character, position: Int) {
                val intent = Intent(this@MainActivity, DetailsHero::class.java)
                val thumbnailUri = hero.thumbnail.path + "." + hero.thumbnail.extension
                val url = "https" + thumbnailUri.subSequence(4, thumbnailUri.length)

                intent.putExtra("NAME_HERO", hero.name)
                if (hero.urls[0].type == "detail") intent.putExtra("URLS", hero.urls[0].url)
                intent.putExtra("DESCRIPTION_HERO", hero.description)
                intent.putExtra("IDENTIFICATOR", hero.id.toString())

                intent.putExtra("IMAGE_HERO", url)

                startActivity(intent)
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            }
        })
        mRecyclerView.adapter = mAdapter


    }


    private fun shapeButton() {
        val radius = resources.getDimension(R.dimen.mtrl_calendar_selection_text_baseline_to_bottom)

        val linearLayout = findViewById<FrameLayout>(R.id.layout)
        linearLayout.isClickable = true

        val shapeAppearanceModel = ShapeAppearanceModel()
            .toBuilder()
            .setTopLeftCorner(CornerFamily.CUT, radius)
            .setBottomRightCorner(CornerFamily.CUT, radius)
            .build()
        val shapeDrawable = MaterialShapeDrawable(shapeAppearanceModel)

        ViewCompat.setBackground(linearLayout, shapeDrawable)

        linearLayout.setOnClickListener {

            it.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
        }
    }

    private fun updateRecyclerView() {
        mRecyclerView.apply {
            mAdapter.updateHeroList(matchedCharacter)
            adapter!!.notifyDataSetChanged()
        }
    }

    private fun initObservables() {
        viewModel.getSuperHeroes().observe(this, {
            character = it

            mAdapter.updateHeroList(character)
            val shimmerFrameLayout: ShimmerFrameLayout = findViewById(R.id.shimmerFrameLayout)
            shimmerFrameLayout.isShimmerStarted
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE

        })
    }
}
