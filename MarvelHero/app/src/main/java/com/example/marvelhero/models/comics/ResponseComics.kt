package com.example.marvelhero.models.comics

data class ResponseComics(
        val code: Int,
        val etag: String,
        val data: DataComics
)