package com.example.marvelhero.models.comics

import com.example.marvelhero.models.Creators
import com.example.marvelhero.models.Thumbnail
import com.example.marvelhero.models.Url


data class Comics(
    val id: Int,
    val title: String,
    val description: String,
    val format: String,
    val pageCount: Int,
    val thumbnail: Thumbnail,
    val urls: Array<Url>,
    val textObjects: Array<TextObjects>,
    val dates: Array<Dates>,
    val prices: Array<Prices>,
    val creators: Creators,

    ) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Comics

                if (id != other.id) return false
                if (title != other.title) return false
                if (description != other.description) return false
                if (format != other.format) return false
                if (pageCount != other.pageCount) return false
                if (thumbnail != other.thumbnail) return false
                if (!urls.contentEquals(other.urls)) return false
                if (!textObjects.contentEquals(other.textObjects)) return false
                if (!dates.contentEquals(other.dates)) return false
                if (!prices.contentEquals(other.prices)) return false
                if (creators != other.creators) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id
                result = 31 * result + title.hashCode()
                result = 31 * result + description.hashCode()
                result = 31 * result + format.hashCode()
                result = 31 * result + pageCount
                result = 31 * result + thumbnail.hashCode()
                result = 31 * result + urls.contentHashCode()
                result = 31 * result + textObjects.contentHashCode()
                result = 31 * result + dates.contentHashCode()
                result = 31 * result + prices.contentHashCode()
                result = 31 * result + creators.hashCode()
                return result
        }

}