package com.example.marvelhero.models

data class Thumbnail(
        var path: String,
        var extension: String
)