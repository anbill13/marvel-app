package com.example.marvelhero.fragments

import android.os.Bundle
import android.util.TypedValue
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.adapters.StoriesAdapter
import com.example.marvelhero.interfacebuttons.ClickListener
import com.example.marvelhero.models.stories.Stories
import com.example.marvelhero.viewmodel.StoriesViewModel
import com.facebook.shimmer.ShimmerFrameLayout

class DetailsStories : Fragment() {


    private lateinit var mRecyclerViewStories: RecyclerView
    private lateinit var mAdapterStories: StoriesAdapter

    private val viewModelStories: StoriesViewModel by lazy {
        ViewModelProviders.of(this).get(StoriesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_others_details_hero, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpRecyclerViewSeries(view)
        shapeButton(view)

    }

    private fun setUpRecyclerViewSeries(view: View) {

        mRecyclerViewStories = view.findViewById(R.id.recyclerView)
        mRecyclerViewStories.setHasFixedSize(true)
        mRecyclerViewStories.layoutManager = GridLayoutManager(view.context, 2)
        mAdapterStories = StoriesAdapter(object : ClickListener<Stories> {
            override fun onClick(view: View, item: Stories, position: Int) {
            }
        })
        mRecyclerViewStories.adapter = mAdapterStories
    }

    private fun shapeButton(view: View) {
        val textButton = view.findViewById<TextView>(R.id.toolbarCategories)

        val nameHero = requireArguments().getString("NAME_HERO")
        val idHero = requireArguments().getString("IDENTIFICATOR")
        initObserver(idHero!!.toInt(), view)

        textButton.text = "${nameHero}'S STORIES"
        textButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28F)
        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)

    }

    private fun initObserver(characterId: Int, view:View) {

        viewModelStories.getStories(characterId).observe(viewLifecycleOwner, {
            val shimmerFrameLayout: ShimmerFrameLayout = view.findViewById(R.id.shimmerFrameLayout)
            shimmerFrameLayout.isShimmerStarted
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
            mAdapterStories.updateStoriesList(it)
        })
    }

}