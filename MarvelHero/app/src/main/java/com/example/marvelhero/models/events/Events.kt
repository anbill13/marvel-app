package com.example.marvelhero.models.events

import com.example.marvelhero.models.Creators
import com.example.marvelhero.models.Thumbnail
import com.example.marvelhero.models.Url


data class Events(
        val id: Int,
        val title: String,
        val description: String,
        val thumbnail: Thumbnail,
        val start: String,
        val end: String,
        val creators: Creators,
        val urls: Array<Url>,

        ) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Events

                if (id != other.id) return false
                if (title != other.title) return false
                if (description != other.description) return false
                if (thumbnail != other.thumbnail) return false
                if (start != other.start) return false
                if (end != other.end) return false
                if (creators != other.creators) return false
                if (!urls.contentEquals(other.urls)) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id
                result = 31 * result + title.hashCode()
                result = 31 * result + description.hashCode()
                result = 31 * result + thumbnail.hashCode()
                result = 31 * result + start.hashCode()
                result = 31 * result + end.hashCode()
                result = 31 * result + creators.hashCode()
                result = 31 * result + urls.contentHashCode()
                return result
        }
}