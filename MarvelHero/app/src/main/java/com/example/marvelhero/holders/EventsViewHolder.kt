package com.example.marvelhero.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.events.Events
import com.example.marvelhero.interfacebuttons.ClickListener

class EventsViewHolder(itemView: View, listener: ClickListener<Events>) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

    lateinit var item: Events
    val resourceURI = itemView.findViewById(R.id.imageComics) as ImageView
    val name = itemView.findViewById(R.id.titleComics) as TextView

        private val clickListener = listener

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {

            clickListener.onClick(v!!,item , adapterPosition)
        }

}