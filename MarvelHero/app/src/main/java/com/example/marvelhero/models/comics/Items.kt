package com.example.marvelhero.models.comics

data class Items(
    val name:String,
    val role:String
)
