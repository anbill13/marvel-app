package com.example.marvelhero.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.marvelhero.R
import com.example.marvelhero.models.stories.Stories
import com.example.marvelhero.extensions.load
import com.example.marvelhero.holders.StoriesViewHolder
import com.example.marvelhero.interfacebuttons.ClickListener

class StoriesAdapter(private val clickListener: ClickListener<Stories>) : RecyclerView.Adapter<StoriesViewHolder>() {
    private val stories = mutableListOf<Stories>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoriesViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item_row_stories,
            parent,
            false
        )
        return StoriesViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: StoriesViewHolder, position: Int) {
        val story = stories[position]

        holder.item = story
        holder.name.text = story.title
        holder.resourceURI.load(story.resourceURI)

    }

    override fun getItemCount(): Int = stories.size

    fun updateStoriesList(stories: List<Stories>) {
        this.stories.clear()
        this.stories.addAll(stories)
        notifyDataSetChanged()
    }
}
