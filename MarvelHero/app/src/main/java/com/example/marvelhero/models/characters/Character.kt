package com.example.marvelhero.models.characters

import com.example.marvelhero.models.Thumbnail
import com.example.marvelhero.models.Url


data class Character(
        val id: Int,
        val name: String,
        val description: String,
        val thumbnail: Thumbnail,
        val urls: Array<Url>
) {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Character

                if (id != other.id) return false
                if (name != other.name) return false
                if (description != other.description) return false
                if (thumbnail != other.thumbnail) return false
                if (!urls.contentEquals(other.urls)) return false

                return true
        }

        override fun hashCode(): Int {
                var result = id
                result = 31 * result + name.hashCode()
                result = 31 * result + description.hashCode()
                result = 31 * result + thumbnail.hashCode()
                result = 31 * result + urls.contentHashCode()
                return result
        }
}