package com.example.marvelhero.models.characters

data class Response(
        val code: Int,
        val etag: String,
        val data: Data
)