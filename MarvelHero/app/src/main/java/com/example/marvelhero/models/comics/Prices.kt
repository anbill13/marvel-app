package com.example.marvelhero.models.comics

data class Prices(
    val type: String,
    val price: String,
)
