package com.example.marvelhero.models

data class Url(
    val type: String,
    val url: String
)