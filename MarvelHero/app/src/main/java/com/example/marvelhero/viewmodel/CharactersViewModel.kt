package com.example.marvelhero.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marvelhero.api.AppModule
import com.example.marvelhero.models.characters.Character
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.await

class CharactersViewModel : ViewModel() {
    var isLoading: Boolean = false
        private set

    fun getSuperHeroes(): LiveData<ArrayList<Character>> {
        val heroList = MutableLiveData<ArrayList<Character>>()

        GlobalScope.launch(Dispatchers.IO) {
            isLoading = true
            val result = AppModule.getCharacters().await()
            heroList.postValue(result.data.results as ArrayList<Character>?)
        }

        isLoading = false
        return heroList
    }

}