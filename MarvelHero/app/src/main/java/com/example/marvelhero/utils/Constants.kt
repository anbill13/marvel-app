package com.example.marvelhero.utils

object Constants {
    const val BASE_URL: String = "https://gateway.marvel.com/v1/public/"
    const val BASE_URL_REQUEST_IMAGE: String = "http://gateway.marvel.com/v1/public/stories/"
    const val END_POINT: String = "characters"
    const val END_POINT_BY_ID: String = "characters/{characterId}"
    const val API_KEY = "fdf05003f2511255c274e260fb44fcbd"
    const val PRIVATE_KEY = "c11a9cc4230ea39d65e3a85010e353b989a1bbe4"
    const val LIMIT: Int = 100
    const val EMPTY_URL = "https://www.marvel.com/characters/3-ds-man-chandler?utm_campaign=apiRef&utm_source=fdf05003f2511255c274e260fb44fcbd"


}